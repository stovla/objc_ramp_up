//
//  main.m
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/11/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
