//
//  NetworkManager.m
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/12/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import "NetworkManager.h"
#import <AFNetworking.h>
#import "TetrisRepo.h"


@implementation NetworkManager

+ (void)fetchApiResultsWithString:(NSString*)urlString success:(void (^)(id result))success failure:(void (^) (NSError *error))failure {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *URL = [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];

    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
            failure(error);
        } else {
            success(responseObject);
        }
    }];
    [dataTask resume];
}

+ (void)fetchImageFromUrl:(NSString*)urlString success:(void (^)(NSData *imageData))success {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data == nil) {
            NSLog(@"Can't load image");
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                success(data);
            });
        }
    }];
    [task resume];
}

@end
