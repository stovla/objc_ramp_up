//
//  NetworkManager.h
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/12/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

#define TETRIS_BASE_API @"https://api.github.com/search/repositories?q="

@interface NetworkManager : NSObject

+ (void)fetchApiResultsWithString:(NSString*)urlString success:(void (^) (id result))success failure:(void (^) (NSError *error))failure;
+ (void)fetchImageFromUrl:(NSString*)urlString success:(void (^)(NSData *imageData))success;

@end
