//
//  ViewController.h
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/11/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"
#import "MyRepoCellCollectionViewCell.h"

@interface ListOfReposCollectionView : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate>


@end

