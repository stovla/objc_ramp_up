//
//  ViewController.m
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/11/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import "ListOfReposCollectionView.h"
#import "NetworkManager.h"
#import <UIScrollView+SVInfiniteScrolling.h>
#import "TetrisRepo.h"

#define PAGINATION @"&per_page=10"
#define DEFAULT_TERM @"tetris"
@interface ListOfReposCollectionView ()

@property (nonatomic) NSMutableArray *repoArray;
@property (nonatomic) int pageCount;
@property (nonatomic) NSMutableString *currentSearchTerm;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation ListOfReposCollectionView

- (void)updateRepoArray:(id)result {
    NSMutableArray *itemsArray = [result objectForKey:@"items"];
    
    for (NSInteger i = 0; i < [itemsArray count]; i++) {
        
        TetrisRepo *repo = [[TetrisRepo alloc] initWithRepoName:itemsArray[i][@"name"]
                                                      loginName:itemsArray[i][@"owner"][@"login"]
                                                     sizeOfRepo:[itemsArray[i][@"size"] intValue]
                                                      avatarURL:itemsArray[i][@"owner"][@"avatar_url"]
                                                        hasWiki:[itemsArray[i][@"has_wiki"] boolValue]];
        [_repoArray addObject:repo];
    }
    NSLog(@"%ld", _repoArray.count);
    if (itemsArray.count >= _repoArray.count) {
        [self.collectionView reloadData];
    } else {
        NSInteger resultSize = [self.repoArray count]; // current size of the data source array
        [self.collectionView performBatchUpdates:^{
            NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
            for (NSInteger i = resultSize; i < resultSize + itemsArray.count; i++) { // adding an array of items
                [arrayWithIndexPaths addObject:[NSIndexPath indexPathForItem:resultSize - 1 inSection:0]]; // indexPath is data source array count - 1
            }
            [self.collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
        } completion:^(BOOL finished) {
            [self.collectionView.infiniteScrollingView stopAnimating];
        }];
    }
    [self.collectionView.infiniteScrollingView stopAnimating];
}

- (void)fetchListBySearching {
    self.pageCount += 1;
    NSString *pageCountString = [NSString stringWithFormat:@"&page=%i", self.pageCount];
    NSString *urlSearchString = [NSString stringWithFormat:@"%@%@%@%@", TETRIS_BASE_API, self.currentSearchTerm, PAGINATION, pageCountString];
    self.collectionView.showsInfiniteScrolling = YES;
    
    if (!AFNetworkReachabilityStatusNotReachable) {
        [NetworkManager fetchApiResultsWithString:urlSearchString success:^(id result) {
            [self updateRepoArray:result];
            self.collectionView.showsInfiniteScrolling = NO;
        } failure:^(NSError *error) {
            [self.collectionView.infiniteScrollingView stopAnimating];
            self.collectionView.showsInfiniteScrolling = NO;
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", error.localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [ac addAction:okButton];
            [self presentViewController:ac animated:YES completion:nil];
            NSLog(@"Failure reason : %@", error.localizedDescription);
        }];
    }
}

- (void)dissmissKeyboard {
    [self.searchBar resignFirstResponder];
}

- (void)setupCollectionView {
    self.collectionView.userInteractionEnabled = YES;
    [self.collectionView addInfiniteScrollingWithActionHandler:^{
        [self fetchListBySearching];
    }];
    [self.collectionView registerClass:[MyRepoCellCollectionViewCell class] forCellWithReuseIdentifier:@"RepoCell"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.currentSearchTerm = [[NSMutableString alloc] initWithFormat:DEFAULT_TERM];
    self.pageCount = 0;
    self.repoArray = [[NSMutableArray alloc] init];
    
    [self setupCollectionView];
    
    [self fetchListBySearching];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.pageCount = 0;
    [self.repoArray removeAllObjects];
    self.currentSearchTerm = (NSMutableString*) searchBar.text;
    [self dissmissKeyboard];
    [self fetchListBySearching];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TetrisRepo *repo = self.repoArray[indexPath.item];
    
    MyRepoCellCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RepoCell" forIndexPath:indexPath];

    cell = [cell initWithFrame:CGRectMake(0, 0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height) repo:repo];
   
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.repoArray count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width - 20, 70);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollViewContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = scrollView.contentOffset.y;
    
    if (scrollOffset + scrollViewHeight == scrollViewContentSizeHeight) {
        [self fetchListBySearching];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TetrisRepo *selected = [self.repoArray objectAtIndex:indexPath.row];
    NSLog(@"selected = %@", selected);
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:@[selected.loginName] applicationActivities:nil];
    [self presentViewController:activity animated:YES completion:nil];
}

@end










