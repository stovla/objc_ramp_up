//
//  ContactsViewController.m
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/16/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import "ContactsViewController.h"
#import "ContactsUI/ContactsUI.h"
#import "ContactsTableViewCell.h"
#import "ContactModel.h"
#import "ListOfReposCollectionView.h"

@interface ContactsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) NSMutableArray *contactsArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contactsArray = [[NSMutableArray alloc] init];
    
    self.navigationItem.title = NSLocalizedString(@"contacts_screen_title", @"My Contacts");
    self.navigationController.navigationBar.prefersLargeTitles = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self scanContacts];
}

- (void)scanContacts {
    NSLog(@"attempting to fetch contacts");
    CNEntityType contactsEntity = CNEntityTypeContacts;;
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:contactsEntity];
    NSLog(@"%ld", (long)[CNContactStore authorizationStatusForEntityType:contactsEntity]);
    if (status == CNAuthorizationStatusNotDetermined) {
            // handle denied and restricted case
            CNContactStore *store = [[CNContactStore alloc] init];
            [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if (error) {
                    NSLog(@"\n<<<<<< Failed to request access: %@", error);
                    [self.tabBarController setSelectedIndex:0];
                    return;
                }
                if (granted) {
                    NSLog(@"\n<<<<<< Access granted");
                    [self getAllContacts];
                } else {
                    [self.tabBarController setSelectedIndex:0];
                    NSLog(@"\n<<<<<< Access for contacts is denied.. >>>>>>>\n");
                }
            }];
        
    } else if (status == CNAuthorizationStatusAuthorized) {
        [self getAllContacts];
    } else if (status == CNAuthorizationStatusDenied) {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Contact Access" message:@"You need to go to settings and grant access to contacts." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *confirmButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                 NSLog(@"OK BUTTON TAPPED BUT THE VIEW DID NOT DISMISS");
            [self.tabBarController setSelectedIndex:0];
        }];
        [ac addAction:confirmButton];
        [self presentViewController:ac animated:YES completion:nil];
    }
}

- (void)getAllContacts {
    [self.contactsArray removeAllObjects];
    
    if ([CNContactStore class]) {
        CNContactStore *contactBook = [[CNContactStore alloc] init];
        NSArray *keys = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        [contactBook enumerateContactsWithFetchRequest:request error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
            [self createContactWithContact:contact];
        }];
    }
}

- (void)createContactWithContact:(CNContact*)contact {
    
    NSString *name = contact.givenName;
    NSString *lastName = contact.familyName;
    NSString *phone = contact.phoneNumbers.firstObject.value.stringValue;
    NSData *imageData;
    if (contact.imageData) {
        imageData = (NSData *)contact.imageData;
    }
    
    ContactModel *customContact = [[ContactModel alloc] initWithContactName:name contactSurname:lastName phoneNumber:phone imageData:imageData];
    [_contactsArray addObject:customContact];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contactsArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactsTableViewCell *cell = [[ContactsTableViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 100)];
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"ContactCell" forIndexPath:indexPath];
    ContactModel *contact = self.contactsArray[indexPath.row];
    cell.contactFullNameLabel.text = [NSString stringWithFormat:@"%@, %@", contact.contactName, contact.contactSurname];
    cell.contactPhoneLabel.text = contact.contactPhone;
    cell.contactImage.image = [UIImage imageWithData:contact.imageData] ?: [UIImage imageNamed:@"user_image"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ContactModel *contact = self.contactsArray[indexPath.row];
    
    NSString *phoneNumber = [contact.contactPhone stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:phoneNumber]];
    NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:phoneNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl options:@{} completionHandler:nil];
        NSLog(@"Can't open with 'telprompt'");
    } else if ([[UIApplication sharedApplication] canOpenURL:phoneFallbackUrl]){
        [[UIApplication sharedApplication] openURL:phoneFallbackUrl options:@{} completionHandler:nil];
        NSLog(@"Can't open with 'tel'");
    } else {
        UIAlertController *calert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Call facility is not avaialable" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           
        }];
        [calert addAction:okButton];
        [self presentViewController:calert animated:YES completion:nil];
    }
}


@end
