//
//  ContactModel.h
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/16/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactModel : NSObject

@property (nonatomic) NSString *contactName;
@property (nonatomic) NSString *contactSurname;
@property (nonatomic) NSString *contactPhone;
@property (nonatomic) NSData *imageData;

-(id)initWithContactName:(NSString*)name contactSurname:(NSString*)surname phoneNumber:(NSString*)contactPhone imageData:(NSData*)imageData;

@end
