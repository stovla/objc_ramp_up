//
//  TetrisRepo.m
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/12/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import "TetrisRepo.h"

@implementation TetrisRepo

- (id)initWithRepoName:(NSString*)repoName loginName:(NSString*)loginName sizeOfRepo:(int)size avatarURL:(NSString*)avatarURL hasWiki:(BOOL)hasWiki {
    self = [super init];
    if (self) {
        self.repoName = repoName;
        self.loginName = loginName;
        self.repoSize = size;
        self.hasWiki = hasWiki ?: NO;
        self.avatarUrl = avatarURL;
    }
    return self;
}

@end
