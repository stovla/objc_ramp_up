//
//  CustomContact.m
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/16/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import "ContactModel.h"

@implementation ContactModel

-(id)initWithContactName:(NSString*)name contactSurname:(NSString*)surname phoneNumber:(NSString*)contactPhone imageData:(NSData*)imageData {
    self = [super init];
    if (self) {
        _contactName = name;
        _contactSurname = surname;
        _contactPhone = contactPhone;
        _imageData = imageData;
    }
    return self;
}

@end
