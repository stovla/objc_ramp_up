//
//  TetrisRepo.h
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/12/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TetrisRepo : NSObject

- (id)initWithRepoName:(NSString*)repoName loginName:(NSString*)loginName sizeOfRepo:(int)size avatarURL:(NSString*)avatarURL hasWiki:(BOOL)hasWiki;

@property (nonatomic, strong) NSString *repoName;
@property (nonatomic, strong) NSString *loginName;
@property (nonatomic) int repoSize;
@property (nonatomic, strong) NSString *avatarUrl;
@property (nonatomic) NSData *imageData;
@property BOOL hasWiki;

@end
