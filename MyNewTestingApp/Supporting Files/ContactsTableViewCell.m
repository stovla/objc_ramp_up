//
//  ContactsTableViewCell.m
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/16/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import "ContactsTableViewCell.h"

@implementation ContactsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
