//
//  MyRepoCellCollectionViewCell.m
//  MyNewTestingApp
//
//  Created by Vlastimir on 15/4/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import "MyRepoCellCollectionViewCell.h"
#import "NetworkManager.h"

@implementation MyRepoCellCollectionViewCell

NSCache *imageCache;

- (id)initWithFrame:(CGRect)frame repo:(TetrisRepo*)repo
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.userImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        self.userImageView.backgroundColor = [UIColor blackColor];
        [self addSubview:self.userImageView];
        
        if ([imageCache objectForKey:repo.avatarUrl]) {
            self.userImageView.image = [UIImage imageWithData:[imageCache objectForKey:repo.avatarUrl]];
        } else {
            [NetworkManager fetchImageFromUrl:repo.avatarUrl success:^(NSData *imageData) {
                [imageCache setObject:imageData forKey:repo.avatarUrl];
                self.userImageView.image = [UIImage imageWithData:imageData];
            }];
        }
        
        
        
        
        self.loginNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(62, 10, frame.size.width - 72, 21)];
        self.loginNameLabel.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.loginNameLabel];
        self.loginNameLabel.text = [NSString stringWithFormat:@"User: <%@> %@", repo.loginName, repo.repoName];
        
        self.sizeOfRepoLabel = [[UILabel alloc] initWithFrame:CGRectMake(62, 41, 100, 21)];
        self.sizeOfRepoLabel.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.sizeOfRepoLabel];
        self.sizeOfRepoLabel.text = [NSString stringWithFormat:@"%i", repo.repoSize];
        
        self.backgroundColor = [repo hasWiki] ? [UIColor blueColor] : [UIColor grayColor];

    }
    return self;
}

//+ (void)fetchImageFromUrlString:(NSString*)urlString {
//    MyRepoCellCollectionViewCell *cell = [[MyRepoCellCollectionViewCell alloc] init];
//    cell.avatarImage = nil;
//    
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setURL:[NSURL URLWithString:urlString]];
//    request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        if (data == nil) {
//            NSLog(@"Can't load image");
//        } else {
//            cell.avatarImage = [UIImage imageWithData:data];
//        }
//    }];
//    [task resume];
//    
//    
//    
//    
//    
//    
//    if ([imageCache objectForKey:urlString]) {
//        cell.avatarImage = (UIImage*)imageCache;
//        return;
//    }
//    
//    NSURL *url = [NSURL URLWithString:urlString];
//    [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//        if (error) {
//            NSLog(@"%@", error.localizedDescription);
//            return;
//        }
//        
//        dispatch_sync(dispatch_get_main_queue(), ^{
//            UIImage *imageToCache = [UIImage imageWithData:data];
//            [imageCache setObject:imageToCache forKey:urlString];
//            cell.avatarImage = imageToCache;
//        });
//    }];
//}

@end
