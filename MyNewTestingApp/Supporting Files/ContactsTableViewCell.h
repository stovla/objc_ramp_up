//
//  ContactsTableViewCell.h
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/16/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *contactImage;
@property (weak, nonatomic) IBOutlet UILabel *contactFullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactPhoneLabel;

@end
