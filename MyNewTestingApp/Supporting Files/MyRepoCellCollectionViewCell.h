//
//  MyRepoCellCollectionViewCell.h
//  MyNewTestingApp
//
//  Created by Vlastimir on 15/4/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TetrisRepo.h"

@interface MyRepoCellCollectionViewCell : UICollectionViewCell

@property (nonatomic) IBOutlet UILabel *loginNameLabel;
@property (nonatomic) IBOutlet UILabel *sizeOfRepoLabel;
@property (nonatomic) IBOutlet UIImageView *userImageView;
@property (nonatomic) UIImage *avatarImage;

- (id)initWithFrame:(CGRect)frame repo:(TetrisRepo*)repo;

@end
