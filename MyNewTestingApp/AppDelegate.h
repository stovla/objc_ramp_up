//
//  AppDelegate.h
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/11/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

