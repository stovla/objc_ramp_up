//
//  AppDelegate.m
//  MyNewTestingApp
//
//  Created by Vlastimir Radojevic on 4/11/18.
//  Copyright © 2018 Vlastimir Radojevic. All rights reserved.
//

#import "AppDelegate.h"
#import <AFNetworkReachabilityManager.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    UNNotificationPresentationOptions presentingOptions = UNNotificationPresentationOptionAlert + UNNotificationPresentationOptionSound;
    completionHandler(presentingOptions);
}


- (void)createNotificationWithTitle:(NSString*)title message:(NSString*)message timeInterval:(NSInteger)interval repating:(BOOL)isRepeating{
    UNUserNotificationCenter *notificationCenter = [UNUserNotificationCenter currentNotificationCenter];
    
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:title arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:message arguments:nil];
    content.sound = [UNNotificationSound defaultSound];
    
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:interval repeats:isRepeating];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"InternetRequest" content:content trigger:trigger];
    [notificationCenter addNotificationRequest:request withCompletionHandler:nil];
}

- (void)setNetworkMonitoring {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown: {
                NSLog(@"INTERNET CONNECTION STATUS UNKNOWN");
                break;
            }
            case AFNetworkReachabilityStatusNotReachable:
                [self createNotificationWithTitle:@"Internet connection" message:@"NO INTERNET CONNECTION" timeInterval:1 repating:NO];
                NSLog(@"NO INTERNET CONNECTION");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"INTERNET CONNECTION BY WIFI");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                [self createNotificationWithTitle:@"Internet connection" message:@"INTERNET CONNECTION BY WWAN" timeInterval:1 repating:NO];
                NSLog(@"INTERNET CONNECTION BY WWAN");
                break;
            default:
                break;
        }
    }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch
    UNUserNotificationCenter *notificationCenter = [UNUserNotificationCenter currentNotificationCenter];
    [notificationCenter requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        NSLog(@"Notification access granted");
    }];
    notificationCenter.delegate = self;
    
    [self setNetworkMonitoring];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"APP DID ENETER BACKGROUND");
    [self createNotificationWithTitle:@"Repeating timer notification" message:@"It will repeat every 5 minutes." timeInterval:300 repating:YES];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
